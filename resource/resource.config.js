import { Injectable } from '@angular/core';
var FormioResourceConfig = /** @class */ (function () {
    function FormioResourceConfig() {
        this.name = '';
        this.form = '';
        this.parents = [];
    }
    FormioResourceConfig.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    FormioResourceConfig.ctorParameters = function () { return []; };
    return FormioResourceConfig;
}());
export { FormioResourceConfig };
//# sourceMappingURL=resource.config.js.map