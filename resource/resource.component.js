import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormioResourceService } from './resource.service';
var FormioResourceComponent = /** @class */ (function () {
    function FormioResourceComponent(service, route) {
        this.service = service;
        this.route = route;
    }
    FormioResourceComponent.prototype.ngOnInit = function () {
        this.service.initialize();
        this.service.loadResource(this.route);
    };
    FormioResourceComponent.decorators = [
        { type: Component, args: [{
                    template: "\n      <ul class=\"nav nav-tabs\">\n        <li class=\"nav-item\"><a class=\"nav-link\" routerLink=\"../\"><i class=\"fa fa-chevron-left glyphicon glyphicon-chevron-left\"></i></a></li>\n        <li class=\"nav-item\" routerLinkActive=\"active\"><a class=\"nav-link\" routerLink=\"view\" routerLinkActive=\"active\">View</a></li>\n        <li class=\"nav-item\" routerLinkActive=\"active\"><a class=\"nav-link\" routerLink=\"edit\" routerLinkActive=\"active\">Edit</a></li>\n        <li class=\"nav-item\" routerLinkActive=\"active\"><a class=\"nav-link\" routerLink=\"delete\" routerLinkActive=\"active\"><span class=\"fa fa-trash glyphicon glyphicon-trash\"></span></a></li>\n      </ul>\n      <router-outlet></router-outlet>"
                },] },
    ];
    /** @nocollapse */
    FormioResourceComponent.ctorParameters = function () { return [
        { type: FormioResourceService, },
        { type: ActivatedRoute, },
    ]; };
    return FormioResourceComponent;
}());
export { FormioResourceComponent };
//# sourceMappingURL=resource.component.js.map