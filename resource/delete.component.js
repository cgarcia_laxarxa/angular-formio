import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormioResourceService } from './resource.service';
var FormioResourceDeleteComponent = /** @class */ (function () {
    function FormioResourceDeleteComponent(service, route, router) {
        this.service = service;
        this.route = route;
        this.router = router;
    }
    FormioResourceDeleteComponent.prototype.onDelete = function () {
        var _this = this;
        this.service.remove().then(function () {
            _this.router.navigate(['../../'], { relativeTo: _this.route });
        });
    };
    FormioResourceDeleteComponent.prototype.onCancel = function () {
        this.router.navigate(['../', 'view'], { relativeTo: this.route });
    };
    FormioResourceDeleteComponent.decorators = [
        { type: Component, args: [{
                    template: "\n      <h3>Are you sure you wish to delete this record?</h3>\n      <div class=\"btn-toolbar\">\n        <button type=\"button\" (click)=\"onDelete()\" class=\"btn btn-danger\" style=\"margin-right: 10px;\">Yes</button>\n        <button type=\"button\" (click)=\"onCancel()\" class=\"btn btn-danger\">No</button>\n      </div>"
                },] },
    ];
    /** @nocollapse */
    FormioResourceDeleteComponent.ctorParameters = function () { return [
        { type: FormioResourceService, },
        { type: ActivatedRoute, },
        { type: Router, },
    ]; };
    return FormioResourceDeleteComponent;
}());
export { FormioResourceDeleteComponent };
//# sourceMappingURL=delete.component.js.map