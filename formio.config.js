import { Injectable } from '@angular/core';
var FormioAppConfig = /** @class */ (function () {
    function FormioAppConfig() {
        this.appUrl = '';
        this.apiUrl = '';
    }
    FormioAppConfig.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    FormioAppConfig.ctorParameters = function () { return []; };
    return FormioAppConfig;
}());
export { FormioAppConfig };
//# sourceMappingURL=formio.config.js.map