import { Injectable } from '@angular/core';
var FormioLoader = /** @class */ (function () {
    function FormioLoader() {
        this.loading = true;
    }
    FormioLoader.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    FormioLoader.ctorParameters = function () { return []; };
    return FormioLoader;
}());
export { FormioLoader };
//# sourceMappingURL=formio.loader.js.map