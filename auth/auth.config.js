import { Injectable } from '@angular/core';
var FormioAuthConfig = /** @class */ (function () {
    function FormioAuthConfig() {
    }
    FormioAuthConfig.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    FormioAuthConfig.ctorParameters = function () { return []; };
    return FormioAuthConfig;
}());
export { FormioAuthConfig };
//# sourceMappingURL=auth.config.js.map